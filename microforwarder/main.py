import argparse
import logging
import sqlite3
import sys

from microforwarder.cli import add_forwarder, list_forwarders, \
    show_forwarder_details, remove_forwarder
from microforwarder.common import User
from microforwarder.forward import make_fetcher, make_poster, do_forwarding


logger = logging.getLogger(__name__)


def main():
    logging.basicConfig(level=logging.INFO)

    db = sqlite3.connect('forwarder.sqlite3')
    try:
        forwarders = db.execute(
            '''
            SELECT source, source_creds, destination, destination_creds
            FROM forwards;
            ''')
    except sqlite3.OperationalError:
        db.execute(
            '''
            CREATE TABLE forwards(
                source VARCHAR(64),
                source_creds TEXT,
                destination VARCHAR(64),
                destination_creds TEXT);
            ''')
        db.execute(
            '''
            CREATE TABLE fetch(
                source VARCHAR(64) PRIMARY KEY,
                last_fetch TIMESTAMP DEFAULT current_timestamp);
            ''')
        db.execute(
            '''
            CREATE TABLE mappings(
                message VARCHAR(64),
                class VARCHAR(64));
            ''')
        forwarders = []

    fetchers = {}
    nb_fetch = 0
    nb_post = 0

    for source, source_creds, destination, destination_creds in forwarders:
        source = User.parse(source)
        destination = User.parse(destination)
        try:
            fetcher = fetchers[source]
        except KeyError:
            fetcher = fetchers[source] = make_fetcher(source, source_creds, db)
            nb_fetch += 1
        poster = make_poster(destination, destination_creds, db)
        fetcher.add_poster(poster)
        nb_post += 1

    logging.info("Read %d fetchers, %d posters",
                 nb_fetch, nb_post)

    parser = argparse.ArgumentParser(
        description="Forwards messages between micro-blogging services")
    subparsers = parser.add_subparsers(title="commands", metavar='')

    add = subparsers.add_parser('add', help="Adds a forwarding configuration")
    add.set_defaults(func=add_forwarder)

    ls = subparsers.add_parser('ls', help="List forwarding configurations")
    ls.set_defaults(func=list_forwarders)

    info = subparsers.add_parser('info',
                                 help="Show details about a configuration")
    info.set_defaults(func=show_forwarder_details)

    rm = subparsers.add_parser('rm', help="Remove a forwarding configuration")
    rm.add_argument('CONFIG_NB', help="Index of the configuration to remove")
    rm.set_defaults(func=remove_forwarder)

    run = subparsers.add_parser('run', help="Run the forwarder program")
    run.set_defaults(func=do_forwarding)

    args = parser.parse_args()
    if getattr(args, 'func', None) is None:
        parser.print_help(sys.stderr)
        sys.exit(2)
    args.func(db=db, fetchers=fetchers)
