from datetime import datetime
import dateutil.parser
import functools
import re


class Message(object):
    """A message from a micro-blogging service.
    """
    def __init__(self, id, source, text, timestamp, coordinates=None,
                 retweet_from=None, retweeted=None, reply_to=None, reply=None):
        self.id = id
        # Who posted this message. If retweet, who retweeted
        self.source = source
        # The content of the message. If retweet, the original content, with no
        # "RT" prefix
        self.text = text.strip()
        # Time of the post/retweet
        self.timestamp = timestamp
        # Physical location associated with that message (longitude, latitude)
        self.coordinates = coordinates
        # If a retweet, this is the author of the original message
        self.retweet_from = retweet_from
        # If a retweet, this is the ID of the original message
        self.retweeted = retweeted
        # If a reply, this is the author of the parent message
        self.reply_to = reply_to
        # ID of the message this replies to, or None
        self.reply = reply

    @property
    def link(self):
        if self.source.service == 'twitter':
            return 'https://twitter.com/%s/status/%s' % (
                self.source.username,
                self.id)
        else:
            return 'https://%s/users/%s/updates/%s' % (
                self.source.domain,
                self.source.username,
                self.id)

    @property
    def retweeted_link(self):
        if self.retweet_from is None:
            return None
        if self.source.service == 'twitter':
            return 'https://twitter.com/%s/status/%s' % (
                self.retweet_from.username,
                self.retweeted)
        else:
            return 'https://%s/users/%s/updates/%s' % (
                self.retweet_from.domain,
                self.retweet_from.username,
                self.retweeted)

    @property
    def reply_link(self):
        if self.reply_to is None:
            return None
        if self.source.service == 'twitter':
            return 'https://twitter.com/%s/status/%s' % (
                self.reply_to.username,
                self.reply)
        else:
            return 'https://%s/users/%s/updates/%s' % (
                self.reply_to.domain,
                self.reply_to.username,
                self.reply)

    @property
    def id_string(self):
        if self.source.service == 'twitter':
            return 'twitter:%s' % self.id
        else:
            return 'mastodon:%s@%s' % (self.id, self.source.domain)

    @property
    def in_reply_to_string(self):
        if self.reply_to is None:
            return None
        if self.source.service == 'twitter':
            return 'twitter:%s' % self.reply
        else:
            return 'mastodon:%s@%s' % (self.reply, self.reply_to.domain)


_domain_re = re.compile(r'^(?:https?://)?([^/:]+)/?$')


@functools.total_ordering
class User(object):
    """A user on a micro-blogging service.
    """
    def __init__(self, service, username, domain=None):
        assert service in ('twitter', 'mastodon')
        assert bool(domain) == (service == 'mastodon')

        self.service = service
        self.username = username
        if domain:
            match = _domain_re.match(domain)
            if match is None:
                raise ValueError("Invalid domain")
            self.domain = match.group(1)
        else:
            self.domain = None

    @staticmethod
    def parse(s):
        if s.startswith('twitter:'):
            return User('twitter', s[8:])
        elif s.startswith('mastodon:'):
            return User.parse_mastodon(s[9:])
        else:
            raise ValueError("Unrecognized user string")

    @staticmethod
    def parse_mastodon(s):
        if s[0] == '@':
            s = s[1:]
        sep = s.index('@')
        return User('mastodon', s[:sep], s[(sep+1):])

    @property
    def account_string(self):
        if self.service == 'twitter':
            return 'twitter:%s' % self.username
        elif self.service == 'mastodon':
            return 'mastodon:%s@%s' % (self.username, self.domain)

    def __str__(self):
        if self.service == 'twitter':
            return '@%s' % self.username
        elif self.service == 'mastodon':
            return '@%s@%s' % (self.username, self.domain)

    def __repr__(self):
        return "User(service=%r, username=%r%s)" % (
            self.service, self.username,
            (", %s" % self.domain) if self.domain is not None else '')

    def __eq__(self, other):
        return (self.service == other.service and
                self.username == other.username and
                self.domain == other.domain)

    def __lt__(self, other):
        return (self.service < other.service or
                self.domain < other.domain or
                self.username < other.username)

    def __hash__(self):
        return hash((self.service, self.username, self.domain))


def parse_date(s):
    date = dateutil.parser.parse(s)
    return datetime(date.year, date.month, date.day,
                    date.hour, date.minute, date.second)


def shorten(text, length):
    """Truncate the text if it is too long.
    """
    if len(text) > length:
        pos = text.rfind(' ', 0, length - 2)
        if pos == -1:
            pos = length - 3
        return text[:pos] + "..."
    else:
        return text
