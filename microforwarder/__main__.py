import os
import sys


try:
    from microforwarder.main import main
except ImportError:
    sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))
    from microforwarder.main import main


main()
