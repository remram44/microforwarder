from mastodon import Mastodon

from .base import Fetcher, Poster


class _MastodonApiCache(object):
    def __init__(self):
        self.cache = {}

    def __call__(self, credentials):
        try:
            return self.cache[credentials]
        except KeyError:
            creds_list = credentials.split('\n')
            api = self.cache[credentials] = Mastodon(
                client_id=creds_list[1],
                client_secret=creds_list[2],
                access_token=creds_list[3],
                ratelimit_method='wait',
                api_base_url=creds_list[0])
            api.account_verify_credentials()
            return api


mastodon_api_cache = _MastodonApiCache()


def mastodon_to_message(toot):
    raise NotImplementedError


class MastodonFetch(Fetcher):
    def __init__(self, source, credentials, db):
        Fetcher.__init__(self, source, db)
        self.api = mastodon_api_cache(credentials)

    def fetch(self):
        pass  # TODO: Mastodon fetch


class MastodonPoster(Poster):
    def __init__(self, destination, credentials, db):
        Poster.__init__(self, db)
        self.destination = destination
        self.api = mastodon_api_cache(credentials)

    def post(self, message):
        pass  # TODO: Mastodon post
