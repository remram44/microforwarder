import sys

from microforwarder.executor import executor
from .fwd_mastodon import MastodonFetch, MastodonPoster
from .fwd_twitter import TwitterFetch, TwitterPoster


def make_fetcher(source, credentials, db):
    if source.service == 'twitter':
        return TwitterFetch(source, credentials, db)
    elif source.service == 'mastodon':
        return MastodonFetch(source, credentials, db)
    else:
        raise ValueError("Don't know how to fetch %r", source)


def make_poster(destination, credentials, db):
    if destination.service == 'twitter':
        return TwitterPoster(destination, credentials, db)
    if destination.service == 'mastodon':
        return MastodonPoster(destination, credentials, db)
    else:
        raise ValueError("Don't know how to post to %r", destination)


def do_forwarding(fetchers, **kwargs):
    if not fetchers:
        sys.stderr.write("It appears that you are running this script for the "
                         "first time.\n"
                         "Please run 'microforwarder add' to add a forwarding "
                         "configuration to the system.\n")
        sys.exit(1)

    executor(fetchers.values())
