from datetime import datetime, timedelta
import logging

from microforwarder.common import parse_date
from microforwarder.executor import Task


logger = logging.getLogger(__name__)


class Fetcher(Task):
    def __init__(self, source, db):
        Task.__init__(self)
        self.posters = []
        self.source = source
        self.db = db
        rows = self.db.execute(
            '''
            SELECT last_fetch FROM fetch
            WHERE source = ?;
            ''',
            (self.source.account_string,))
        try:
            last_fetch, = next(rows)
        except StopIteration:
            logger.info("Initializing last_fetch to an hour ago")
            self.last_fetch = datetime.utcnow() - timedelta(hours=1)
            self.db.execute(
                '''
                INSERT OR REPLACE INTO fetch(source, last_fetch)
                VALUES(?, ?);
                ''',
                (self.source.account_string, self.last_fetch))
            self.db.commit()
        else:
            self.last_fetch = parse_date(last_fetch)

    def add_poster(self, poster):
        self.posters.append(poster)

    def execute(self):
        # Fetch again in 10 minutes
        self.due_date = datetime.utcnow() + timedelta(minutes=10)
        tasks = [self]

        messages = self.fetch()

        if messages:
            messages = sorted(messages, key=lambda m: m.date)

            # Post new messages
            for poster in self.posters:
                poster.post_messages(messages)
                tasks.append(poster)

        # Update last fetch date
        self.last_fetch = datetime.utcnow()
        self.db.execute(
            '''
            INSERT OR REPLACE INTO fetch(source, last_fetch)
            VALUES(?, ?);
            ''',
            (self.source.account_string, self.last_fetch))
        self.db.commit()

        return tasks

    def fetch(self):
        raise NotImplementedError


class Poster(Task):
    def __init__(self, db):
        Task.__init__(self)
        self.db = db
        self.queue = []

    def post_messages(self, messages):
        self.queue.extend(messages)

    def execute(self):
        if not self.queue:
            return

        self.post(self.queue[0])
        self.queue.pop(0)

        if self.queue:
            return [self]

    def post(self, message):
        raise NotImplementedError
