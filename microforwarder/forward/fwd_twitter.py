import logging
import twitter

from microforwarder.common import Message, User, parse_date, shorten
from .base import Fetcher, Poster


logger = logging.getLogger(__name__)


class _TwitterApiCache(object):
    def __init__(self):
        self.cache = {}

    def __call__(self, credentials):
        try:
            return self.cache[credentials]
        except KeyError:
            creds_list = credentials.split('\n')
            api = self.cache[credentials] = twitter.Api(
                consumer_key=creds_list[0],
                consumer_secret=creds_list[1],
                access_token_key=creds_list[2],
                access_token_secret=creds_list[3])
            api.VerifyCredentials()
            return api


twitter_api_cache = _TwitterApiCache()


def twitter_to_message(tweet):
    text = tweet.text
    retweet_from = retweeted = None
    if tweet.retweeted_status:
        retweet_from = User('twitter',
                            tweet.retweeted_status.user.screen_name)
        retweeted = tweet.retweeted_status.id_str
        text = tweet.retweeted_status.text

    coordinates = None
    if tweet.coordinates:
        coordinates = tuple(tweet.coordinates['coordinates'])

    reply_to = reply = None
    if tweet.in_reply_to_status_id_str:
        reply_to = User('twitter',
                        tweet.in_reply_to_screen_name)
        reply = tweet.in_reply_to_status_id_str

    return Message(
        id=tweet.id_str,
        source=User('twitter', tweet.user.screen_name),
        text=text,
        timestamp=parse_date(tweet.created_at),
        coordinates=coordinates,
        retweet_from=retweet_from,
        retweeted=retweeted,
        reply_to=reply_to,
        reply=reply)


class TwitterFetch(Fetcher):
    def __init__(self, source, credentials, db):
        Fetcher.__init__(self, source, db)
        self.api = twitter_api_cache(credentials)

    def fetch(self):
        # Get tweets
        max_id = None
        tweets = []
        while True:
            logger.info("Getting tweets...")
            new_tweets = self.api.GetUserTimeline(include_rts=True,
                                                  exclude_replies=False,
                                                  max_id=max_id)
            last_date = parse_date(new_tweets[-1].created_at)
            if last_date > self.last_fetch:
                # The oldest tweet we got is more recent than the last time we
                # fetched. There might have been more tweets than that, keep
                # fetching
                tweets.extend(new_tweets)
                max_id = new_tweets[-1].id
            else:
                # We got all the tweets
                # Add the ones more recent than the last fetch to the list
                for i, tweet in enumerate(new_tweets):
                    date = parse_date(new_tweets[-1].created_at)
                    if date <= self.last_fetch:
                        tweets.extend(new_tweets[:i])
                        break
                break

        if tweets:
            logger.info("Got %d tweets", len(tweets))
            return [twitter_to_message(tweet) for tweet in tweets]


class TwitterPoster(Poster):
    def __init__(self, destination, credentials, db):
        Poster.__init__(self, db)
        self.destination = destination
        self.api = twitter_api_cache(credentials)

    def post(self, message):
        logger.info("Forwarding a message: %r", message)

        if message.retweet_from:
            logger.info("Posting a retweet")

            retweet = link = None
            # In the insane case we're forwarding from Twitter to Twitter
            # and retweeting someone we're forwarding (possibly ourself)
            # avoid retweeting the original, retweet the forwarded
            # message. If forwarding multiple times, out of luck.
            avoid = ''
            if message.source.service == 'twitter':
                avoid = ' AND message <> \'twitter:{}\''.format(
                    message.retweeted)
            rows = self.db.execute(
                '''
                SELECT message
                FROM mappings
                WHERE class=(
                    SELECT class
                    FROM mappings
                    WHERE message=?
                ) AND message LIKE 'twitter:%'{};
                '''.format(avoid),
                (message.retweeted,))
            try:
                retweet, = next(rows)
            except StopIteration:
                # TODO: Defer current task if the message should exist (we know
                # of a forwarding so the retweeted message will be forwarded)
                if message.source.service == 'twitter':
                    retweet = message.retweeted
                    logger.info("Using the same retweet")
                else:
                    link = message.retweeted_link
                    logger.info("No forwarding found, will link to original")
            else:
                retweet = retweet[8:]
                logger.info("Found a forwarded message, will retweet %s",
                            retweet)

            if retweet:
                msg = self.api.PostRetweet(retweet).id_str
            else:
                text = 'RT %s %s' % (link, shorten(message.text, 113))
                logger.info("Posting as '%s' (%d chars)",
                            text, len(text))
                msg = self.api.PostUpdate(
                    status=text).id_str
        else:
            longitude = latitude = None
            if message.coordinates:
                longitude, latitude = message.coordinates

            text = message.text

            reply_to = reply_link = None
            if message.reply_to:
                logger.info("Posting a reply")

                # In the insane case we're forwarding from Twitter to Twitter
                # and replying to someone we're forwarding (possibly ourself)
                # avoid replying to the original, reply to the forwarded
                # message. If forwarding multiple times, out of luck.
                avoid = ''
                if message.source.service == 'twitter':
                    avoid = ' AND message <> \'twitter:{}\''.format(
                        message.reply_to)
                rows = self.db.execute(
                    '''
                    SELECT message
                    FROM mappings
                    WHERE class=(
                        SELECT class
                        FROM mappings
                        WHERE message=?
                    ) AND message LIKE 'twitter:%'{};
                    '''.format(avoid),
                    (message.reply,))
                try:
                    reply_to, = next(rows)
                except StopIteration:
                    # TODO: Defer current task if the message should exist (we
                    # know of a forwarding so the parent message will be
                    # forwarded)
                    if message.source.service == 'twitter':
                        reply_to = message.reply
                        logger.info("Using the same parent")
                    else:
                        reply_link = message.reply_link
                        logger.info("No forwarding found, will link to parent")
                else:
                    reply_to = reply_to[8:]
                    logger.info("Found a forwarded message, will reply to %s",
                                reply_to)
            else:
                logger.info("Posting standalone message")

            if reply_link:
                if len(text) > 116:
                    text = '%s %s %s' % (reply_link,
                                         shorten(text, 92),
                                         message.link)
                    logger.info("Too long with link, will post '%s' "
                                "(%d chars)",
                                text, len(text))
                else:
                    text = '%s %s' % (reply_link, text)
                    logger.info("Text fits with link, will post '%s' "
                                "(%d chars)",
                                text, len(text))
            else:
                if len(text) > 140:
                    text = '%s %s' % (shorten(text, 116), message.link)
                    logger.info("Text doesn't fit, will link to original. "
                                "Posting '%s' (%d chars)",
                                text, len(text))
                else:
                    logger.info("Text fits, will post '%s' (%d chars)",
                                text, len(text))

            msg = self.api.PostUpdate(
                status=text,
                in_reply_to_status_id=reply_to,
                longitude=longitude,
                latitude=latitude).id_str

        message_old = message.id_string
        message_new = 'twitter:%s' % msg
        self.db.executemany(
            '''
            INSERT INTO mappings(message, class)
            VALUES(?, ?);
            ''',
            [(message_new, message_new), (message_old, message_new)])
