import getpass
import sys

from microforwarder.common import User


def ask_yesno(prompt):
    """Ask a yes/no question and wait for a valid answer.
    """
    answer = ''
    while answer not in ('y', 'n'):
        answer = raw_input(prompt).strip().lower()
    return answer == 'y'


def select_credentials(db, service):
    """Select credentials among the ones already stored in the database.

    When we need credentials for a given service, we go through the database
    and list the credentials we already have. The user may choose one of those
    or enter new credentials. If there are no credentials for the given service
    in the database, we just ask for new credentials.
    """
    credentials = db.execute(
        '''
        SELECT what, name, creds
        FROM (
            SELECT 'source' AS what, source AS name, source_creds AS creds
            FROM forwards
            UNION ALL
            SELECT 'destination' AS what, destination AS name,
                destination_creds AS creds
            FROM forwards
        )
        WHERE name LIKE '{0}:%'
        ORDER BY creds;
        '''.format(service))
    creds_list = []
    last_creds = None
    for what, name, creds in credentials:
        account = '@' + name[(len(service) + 1):]
        if what == 'source':
            desc = "used to fetch %s" % account
        else:
            desc = "used to post to %s" % account
        if creds == last_creds:
            print("    and %s" % desc)
        else:
            creds_list.append(creds)
            last_creds = creds
            print("[% 2d] Credentials %s" % (len(creds_list), desc))
    if creds_list:
        print("[ 0] Enter new credentials\n")
        print("You already have %s credentials configured. Please select one "
              "of the accounts above or 0 to enter new credentials.",
              service)
        creds_nb = -1
        while creds_nb < 0 or creds_nb > len(creds_list):
            try:
                creds_nb = int(raw_input("> ").strip())
            except ValueError:
                creds_nb = -1
        if creds_nb > 0:
            return creds_list[creds_nb - 1]
    return None


def get_twitter_credentials(db):
    """Ask for Twitter credentials, from a manually created OAuth App.
    """
    import twitter

    print("Selecting Twitter API credentials.\n")

    api = api_user = None
    creds = select_credentials(db, 'twitter')
    if creds:
        creds = creds.split('\n')
        api = twitter.Api(
            consumer_key=creds[0],
            consumer_secret=creds[1],
            access_token_key=creds[2],
            access_token_secret=creds[3])
        r = api.VerifyCredentials()
        api_user = User('twitter', r.screen_name)
        print("Using selected credentials (account: %s)" % api_user)
    else:
        print("To talk to Twitter, you'll need a Twitter API key.\n")
        print("Usually, the application's creator is supposed to do that, "
              "but with an application that you'll host yourself, I cannot "
              "distribute my own API keys to you.\n")
        print("You'll need to register an app on "
              "https://apps.twitter.com/ . You may have to add a phone "
              "number to you Twitter account to be able to do this.")
        print("Make sure to allow your app write "
              "permissions, if you want to use those credentials for "
              "posting from another account to this Twitter account.\n")
        print("Once you are done, go to your app's \"Keys and Tokens\" "
              "page and enter the info from there below.\n")

        twitter_works = False
        while not twitter_works:
            creds = (
                raw_input(
                    "Twitter Consumer Key (API Key): ").strip(),
                raw_input(
                    "Twitter Consumer Secret (API Secret): ").strip(),
                raw_input(
                    "Twitter Access Token: ").strip(),
                raw_input(
                    "Twitter Access Token Secret: ").strip())
            print("\nAlright, trying to connect to Twitter with those "
                  "credentials...")
            try:
                twitter_works = True
                api = twitter.Api(
                    consumer_key=creds[0],
                    consumer_secret=creds[1],
                    access_token_key=creds[2],
                    access_token_secret=creds[3])
                r = api.VerifyCredentials()
                api_user = User('twitter', r.screen_name)
            except Exception:
                twitter_works = False

            if not twitter_works:
                print("That didn't work. Check if you copied everything "
                      "correctly and make sure you are connected to the "
                      "Internet.\n")

        print("Great! Twitter access works.\n")
    return '\n'.join(creds), api, api_user


def get_twitter_source(db):
    """Select a Twitter user to forward from.
    """
    credentials, api, api_user = get_twitter_credentials(db)

    screen_name = None
    while not screen_name:
        screen_name = raw_input(
            "Enter the Twitter username to read from "
            "(leave empty for %s): " % api_user).strip()
        if not screen_name:
            screen_name = api_user.username
        elif screen_name.startswith('@'):
            screen_name = screen_name[1:]
    source = User('twitter', screen_name)

    return source, credentials, api_user


def get_twitter_destination(db, twitter_creds):
    """Select a Twitter user to forward to.

    We can only forward to the user whose credentials we are using.

    :param twitter_creds: The Twitter credentials we just entered for the
        source, or None. If set, we will ask if the same credentials should be
        used for the destination, since it is likely that the user only wants
        to use one Twitter account (in the case he is forwarding from Twitter
        to Twitter).
    """
    if twitter_creds is not None:
        if ask_yesno(
                "You just entered credentials for the account @%s. Is that "
                "the account you want to use to post messages as well as "
                "fetch? [y/n] " % twitter_creds[1]):
            credentials, api_user = twitter_creds
        else:
            credentials, api, api_user = get_twitter_credentials(db)
    else:
        credentials, api, api_user = get_twitter_credentials(db)
    destination = api_user

    return destination, credentials, api_user


def get_mastodon_credentials(db):
    """Ask for Mastodon credentials.

    An App can be created automatically from the user's username/password.
    """
    from mastodon import Mastodon

    print("Selecting Mastodon API credentials.\n")

    api = api_user = None
    creds = select_credentials(db, 'mastodon')
    if creds:
        creds = creds.split('\n')
        api = Mastodon(
            client_id=creds[1],
            client_secret=creds[2],
            access_token=creds[3],
            ratelimit_method='wait',
            api_base_url=creds[0])
        username = api.account_verify_credentials()['username']
        domain = creds[0]
        api_user = User('mastodon', username, domain)
        print("Using selected credentials (account: %s)" % api_user)
    else:
        print("microforwarder logs to your account as an application, using "
              "OAuth credentials. To set those up, I will ask for your "
              "account information once, so I can create the application and "
              "memorize the key. Your password will NOT be saved or used "
              "again.\n")

        mastodon_works = False
        while not mastodon_works:
            base_url = raw_input("Mastodon server (leave empty for "
                                 "mastodon.social): ").strip()
            if base_url == '':
                base_url = 'mastodon.social'
            if not (base_url.startswith('http:') or
                    base_url.startswith('https:')):
                base_url = 'https://' + base_url
            username = raw_input(
                "Mastodon username (usually your email): ").strip()
            password = getpass.getpass("Mastodon password: ").strip()

            print("Creating app...")
            try:
                client_id, client_secret = Mastodon.create_app(
                    'microforwarder',
                    scopes=['read', 'write'],
                    api_base_url=base_url)
            except Exception:
                print("app creating failed. This shouldn't happen. Please "
                      "retry from the start, and if it keeps failing, submit "
                      "a bug report at "
                      "https://github.com/remram44/microforwarder/issues")
                continue
            print("Logging in...")
            try:
                mastodon_works = True
                api = Mastodon(
                    client_id=client_id,
                    client_secret=client_secret,
                    api_base_url=base_url)
                access_token = api.log_in(username=username, password=password,
                                          scopes=['read', 'write'])
            except Exception:
                mastodon_works = False
                print("Logging in didn't work. Check if you entered the "
                      "information correctly and your connection to the "
                      "server works.\n")
            username = api.account_verify_credentials()['username']
            api_user = User('mastodon', username, base_url)

        creds = '\n'.join((base_url, client_id, client_secret, access_token))

    return creds, api, api_user


def get_mastodon_source(db):
    """Select a Mastodon user to forward from.
    """
    credentials, api, api_user = get_mastodon_credentials(db)

    username = None
    while not username:
        username = raw_input(
            "Enter the Mastodon username to read from (leave empty for %s): " %
            api_user).strip()
        if not username:
            username = str(api_user)
    source = User.parse_mastodon(username)

    return source, credentials, api_user


def get_mastodon_destination(db, mastodon_creds):
    """Select a Mastodon user to forward to.

    We can only forward to the user whose credentials we are using.

    :param mastodon_creds: The Mastodon credentials we just entered for the
        source, or None. If set, we will ask if the same credentials should be
        used for the destination, since it is likely that the user only wants
        to use one Mastodon account (in the case he is forwarding from Mastodon
        to Mastodon).
    """
    if mastodon_creds is not None:
        if ask_yesno(
                "You just entered credentials for the account %s. Is that the "
                "account you want to use to post messages as well as fetch ? "
                "[y/n] " % mastodon_creds[1]):
            credentials, api_user = mastodon_creds
        else:
            credentials, api, api_user = get_mastodon_credentials(db)
    else:
        credentials, api, api_user = get_mastodon_credentials(db)
    destination = api_user

    return destination, credentials, api_user


def add_forwarder(db, **kwargs):
    """Interactive wizard to add a forwarding configuration.

    This will ask for the source and target of the forwarding, along with the
    credentials for the account. Already known credentials will be offered for
    reuse.
    """
    print("This wizard will allow you to add a forwarding configuration to "
          "the system. When this is done, restart microforwarder and messages "
          "will be forwarded according to your settings.\n\n")

    twitter_creds = None
    mastodon_creds = None

    service = None
    while service not in ('twitter', 'mastodon'):
        service = raw_input(
            "Select the source service: [twitter/mastodon] ").strip()
    if service == 'twitter':
        source, source_creds, source_creds_user = get_twitter_source(db)
        twitter_creds = source_creds, source_creds_user
    else:
        source, source_creds, source_creds_user = get_mastodon_source(db)
        mastodon_creds = source_creds, source_creds_user

    service = None
    while service not in ('twitter', 'mastodon'):
        service = raw_input(
            "Select the destination service: [twitter/mastodon] ").strip()
    if service == 'twitter':
        destination, dest_creds, dest_creds_user = get_twitter_destination(
            db, twitter_creds)
    else:
        destination, dest_creds, dest_creds_user = get_mastodon_destination(
            db, mastodon_creds)

    print("\nForwarding from:")
    if source.service == 'twitter':
        print("  Service: Twitter\n"
              "  Screen name: %s\n"
              "  Using credentials from: %s" % (
                  source, source_creds_user))
    else:
        print("  Service: Mastodon\n"
              "  URL: %s\n"
              "  Username: %s\n"
              "  Using credentials from: %s" % (
                  source_creds.split('\n', 1)[0],
                  source, source_creds_user))

    if source == destination:
        print("You entered the same source and destination accounts. This "
              "cannot work.")
        sys.exit(1)

    print("Forwarding to:")
    if destination.service == 'twitter':
        print("  Service: Twitter\n"
              "  Screen name: %s\n"
              "  Using credentials from: %s" % (
                  destination, dest_creds_user))
    else:
        print("  Service: Mastodon\n"
              "  URL: %s\n"
              "  Username: %s\n"
              "  Using credentials from: %s" % (
                  dest_creds.split('\n', 1)[0],
                  destination, dest_creds_user))
    print("")

    both = False
    if source_creds_user == source:
        both = ask_yesno("Should messages be forwarded back as well? [y/n] ")
    else:
        print("Cannot set up bidirectional forwarding because the source "
              "credentials don't match the source account.")
    print("")

    if not ask_yesno("Is this correct? [y/n] "):
        sys.exit(1)

    db.execute(
        '''
        INSERT INTO forwards(source, source_creds,
                             destination, destination_creds)
        VALUES(?, ?, ?, ?);
        ''',
        (source.account_string, source_creds,
         destination.account_string, dest_creds))
    if both:
        db.execute(
            '''
            INSERT INTO forwards(source, source_creds,
                                 destination, destination_creds)
            VALUES(?, ?, ?, ?);
            ''',
            (destination.account_string, dest_creds,
             source.account_string, source_creds))
    db.commit()


def list_forwarders(fetchers, **kwargs):
    for fetcher in fetchers.values():
        print("FETCH: %s %s" % (fetcher.source.service, fetcher.source))
        for poster in fetcher.posters:
            print("  POST: %s %s" % (poster.destination.service,
                                     poster.destination))


def show_forwarder_details(db, **kwargs):
    raise NotImplementedError  # TODO: info CLI


def remove_forwarder(db, **kwargs):
    raise NotImplementedError  # TODO: rm CLI
