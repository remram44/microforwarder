import bisect
from datetime import datetime, timedelta
import functools
import logging
import time


logger = logging.getLogger(__name__)


RETRY = [5, 30, 150, 180, 300]


class TaskThrottled(Exception):
    pass


@functools.total_ordering
class Task(object):
    retry_count = 0

    def __init__(self, due_date=datetime.utcnow() - timedelta(hours=1)):
        self.due_date = due_date

    def __eq__(self, other):
        return self.due_date == other.due_date

    def __lt__(self, other):
        return self.due_date < other.due_date

    def execute(self):
        logger.error("Empty execute(): %s", self.__class__.__name__)


def executor(tasks):
    tasks = sorted(tasks)
    task_set = set(tasks)

    throttle = 0

    while tasks:
        now = datetime.utcnow()
        task = tasks.pop(0)
        task_set.discard(task)
        if task.due_date > now:
            wait = (task.due_date - now).total_seconds()
            logger.info("Sleeping %f seconds...", wait)
            time.sleep(wait)
        logger.info("----------\n%s", task)
        try:
            ret = task.execute()
        except TaskThrottled:
            throttle += 1
            logger.info("Throttling, will wait %f")
            idx = bisect.bisect(tasks, task)
            tasks.insert(idx, task)
            task_set.add(task)
        except Exception:
            logger.exception("Error from a task")
            task.retry_count += 1
            if task.retry_count >= 5:
                logger.error("TASK FAILED FOR THE %d TIME!\n%s",
                             task.retry_count, task)
            else:
                wait = RETRY[task.retry_count]
                logger.warning("Task failed for the %d time, "
                               "rescheduling in %d seconds\n%s",
                               task.retry_count, wait, task)
                task.due_date = datetime.utcnow() + timedelta(seconds=wait)
                idx = bisect.bisect(tasks, task)
                tasks.insert(idx, task)
                task_set.add(task)
        else:
            throttle -= 1
            if ret is not None:
                for new_task in ret:
                    idx = bisect.bisect(tasks, new_task)
                    tasks.insert(idx, new_task)
                    task_set.add(task)
            logger.info("%d new tasks, %d total", len(ret), len(tasks))

        throttle = max(0, min(4, throttle))
        time.sleep(RETRY[throttle])

    logger.info("No more tasks to be run")
