Microforwarder
==============

This is a Python 2 application that forwards messages between different accounts on micro-blogging platforms. It can be used to forward message between your Twitter and Mastodon account, one-way or both ways, but can also forward between Mastodon account etc.

It is currently work-in-progress (not yet usable).

It is inspired from [halcy's MastodonToTwitter script](https://github.com/halcy/MastodonToTwitter), though it is a bit more complex:

* It can forward between two Twitter accounts or two Mastodon accounts as well
* It can handle multiple pairs of accounts with one program instance
* It will keep a mapping of forwarded message it knows, to preserve retweets, replies, and threads
* it can know about other such forwardings, allowing your reply to another user's toot on Mastodon to be forwarded to your Twitter as a reply to the other user's forwarded tweet (it will search for said forwarded tweet)
